# # 1. Write a Python function to find the maximum of three numbers.
# def MaxTwoNumbers(a,b):
#     if a>b:
#         return a
#     else:
#         return b
# def MaxThreeNumbers(a,b,c):
#     return MaxTwoNumbers(MaxTwoNumbers(a,b),c)    

# print(MaxThreeNumbers(7,34,56))

# # 2. Write a Python function to sum all the numbers in a list.
# def SumOfAllNumbers(n):
#     s=0
#     for x in n:
#         s=s+x
#     print(s)    
    
    
# SumOfAllNumbers([1,2,3,4,5,6,7,8])    

# # 3. Write a Python function to multiply all the numbers in a list.
# def MulAllNumbers(n):
#     s=1
#     for x in n:
#         s=s*x
#     print(s)    
    
    
# MulAllNumbers([1,2,3,4,5,6,7,8])    

# 4. Write a Python program to reverse a string.
def reverse(n):
    if n:
        print(n[::-1])
        
reverse("hello")        

# # 5. Write a Python function to calculate the factorial of a number (a non-negative integer). 
# # The function accepts the number as an argument.
# def fuctorial(n):
#     if n==0:
#         return 1 
#     return n*fuctorial(n-1)

# print(fuctorial(5))